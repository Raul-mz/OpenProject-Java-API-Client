# UserPreferencesApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiV3MyPreferencesGet**](UserPreferencesApi.md#apiV3MyPreferencesGet) | **GET** /api/v3/my_preferences | Show my preferences
[**apiV3MyPreferencesPatch**](UserPreferencesApi.md#apiV3MyPreferencesPatch) | **PATCH** /api/v3/my_preferences | Update UserPreferences


<a name="apiV3MyPreferencesGet"></a>
# **apiV3MyPreferencesGet**
> apiV3MyPreferencesGet()

Show my preferences



### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.UserPreferencesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

UserPreferencesApi apiInstance = new UserPreferencesApi();
try {
    apiInstance.apiV3MyPreferencesGet();
} catch (ApiException e) {
    System.err.println("Exception when calling UserPreferencesApi#apiV3MyPreferencesGet");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

<a name="apiV3MyPreferencesPatch"></a>
# **apiV3MyPreferencesPatch**
> apiV3MyPreferencesPatch(body)

Update UserPreferences

When calling this endpoint the client provides a single object, containing the properties that it wants to change, in the body.

### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.UserPreferencesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

UserPreferencesApi apiInstance = new UserPreferencesApi();
Body4 body = new Body4(); // Body4 | 
try {
    apiInstance.apiV3MyPreferencesPatch(body);
} catch (ApiException e) {
    System.err.println("Exception when calling UserPreferencesApi#apiV3MyPreferencesPatch");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Body4**](Body4.md)|  | [optional]

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

