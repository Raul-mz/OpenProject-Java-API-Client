# RevisionsApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiV3RevisionsIdGet**](RevisionsApi.md#apiV3RevisionsIdGet) | **GET** /api/v3/revisions/{id} | View revision


<a name="apiV3RevisionsIdGet"></a>
# **apiV3RevisionsIdGet**
> apiV3RevisionsIdGet(id)

View revision



### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.RevisionsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

RevisionsApi apiInstance = new RevisionsApi();
Integer id = 56; // Integer | Revision id
try {
    apiInstance.apiV3RevisionsIdGet(id);
} catch (ApiException e) {
    System.err.println("Exception when calling RevisionsApi#apiV3RevisionsIdGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**| Revision id |

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

