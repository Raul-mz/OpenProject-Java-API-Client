# TypesApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiV3ProjectsProjectIdTypesGet**](TypesApi.md#apiV3ProjectsProjectIdTypesGet) | **GET** /api/v3/projects/{project_id}/types | List types available in a project
[**apiV3TypesGet**](TypesApi.md#apiV3TypesGet) | **GET** /api/v3/types | List all Types
[**apiV3TypesIdGet**](TypesApi.md#apiV3TypesIdGet) | **GET** /api/v3/types/{id} | View Type


<a name="apiV3ProjectsProjectIdTypesGet"></a>
# **apiV3ProjectsProjectIdTypesGet**
> WPTypes apiV3ProjectsProjectIdTypesGet(projectId)

List types available in a project

This endpoint lists the types that are *available* in a given project.

### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.TypesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

TypesApi apiInstance = new TypesApi();
Integer projectId = 56; // Integer | ID of the project whoose types will be listed
try {
    WPTypes result = apiInstance.apiV3ProjectsProjectIdTypesGet(projectId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling TypesApi#apiV3ProjectsProjectIdTypesGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **projectId** | **Integer**| ID of the project whoose types will be listed |

### Return type

[**WPTypes**](WPTypes.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

<a name="apiV3TypesGet"></a>
# **apiV3TypesGet**
> WPTypes apiV3TypesGet()

List all Types



### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.TypesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

TypesApi apiInstance = new TypesApi();
try {
    WPTypes result = apiInstance.apiV3TypesGet();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling TypesApi#apiV3TypesGet");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**WPTypes**](WPTypes.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

<a name="apiV3TypesIdGet"></a>
# **apiV3TypesIdGet**
> WPType apiV3TypesIdGet(id)

View Type



### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.TypesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

TypesApi apiInstance = new TypesApi();
Integer id = 56; // Integer | type id
try {
    WPType result = apiInstance.apiV3TypesIdGet(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling TypesApi#apiV3TypesIdGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**| type id |

### Return type

[**WPType**](WPType.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

