
# WPTypesEmbedded

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**elements** | [**List&lt;WPType&gt;**](WPType.md) |  |  [optional]



