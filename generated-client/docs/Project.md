
# Project

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  |  [optional]
**name** | **String** |  |  [optional]
**identifier** | **String** |  |  [optional]
**spentOn** | [**LocalDate**](LocalDate.md) |  |  [optional]
**createdAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**updatedAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**embedded** | [**TimeEntryEmbedded**](TimeEntryEmbedded.md) |  |  [optional]



