# SchemasApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiV3ExampleSchemaGet**](SchemasApi.md#apiV3ExampleSchemaGet) | **GET** /api/v3/example/schema | view the schema


<a name="apiV3ExampleSchemaGet"></a>
# **apiV3ExampleSchemaGet**
> apiV3ExampleSchemaGet()

view the schema

This is an example of how a schema might look like. Note that this endpoint does not exist in the actual implementation.

### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.SchemasApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

SchemasApi apiInstance = new SchemasApi();
try {
    apiInstance.apiV3ExampleSchemaGet();
} catch (ApiException e) {
    System.err.println("Exception when calling SchemasApi#apiV3ExampleSchemaGet");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

