# QueryFiltersApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiV3QueriesFiltersIdGet**](QueryFiltersApi.md#apiV3QueriesFiltersIdGet) | **GET** /api/v3/queries/filters/{id} | View Query Filter


<a name="apiV3QueriesFiltersIdGet"></a>
# **apiV3QueriesFiltersIdGet**
> apiV3QueriesFiltersIdGet(id)

View Query Filter

Retreive an individual QueryFilter as identified by the id parameter.

### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.QueryFiltersApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

QueryFiltersApi apiInstance = new QueryFiltersApi();
String id = "id_example"; // String | QueryFilter identifier.
try {
    apiInstance.apiV3QueriesFiltersIdGet(id);
} catch (ApiException e) {
    System.err.println("Exception when calling QueryFiltersApi#apiV3QueriesFiltersIdGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| QueryFilter identifier. |

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

