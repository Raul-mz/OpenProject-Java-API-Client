# CategoriesApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiV3CategoriesIdGet**](CategoriesApi.md#apiV3CategoriesIdGet) | **GET** /api/v3/categories/{id} | View Category
[**apiV3ProjectsProjectIdCategoriesGet**](CategoriesApi.md#apiV3ProjectsProjectIdCategoriesGet) | **GET** /api/v3/projects/{project_id}/categories | List categories of a project


<a name="apiV3CategoriesIdGet"></a>
# **apiV3CategoriesIdGet**
> apiV3CategoriesIdGet(id)

View Category



### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.CategoriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

CategoriesApi apiInstance = new CategoriesApi();
Integer id = 56; // Integer | category id
try {
    apiInstance.apiV3CategoriesIdGet(id);
} catch (ApiException e) {
    System.err.println("Exception when calling CategoriesApi#apiV3CategoriesIdGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**| category id |

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

<a name="apiV3ProjectsProjectIdCategoriesGet"></a>
# **apiV3ProjectsProjectIdCategoriesGet**
> apiV3ProjectsProjectIdCategoriesGet(projectId)

List categories of a project



### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.CategoriesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

CategoriesApi apiInstance = new CategoriesApi();
Integer projectId = 56; // Integer | ID of the project whoose categories will be listed
try {
    apiInstance.apiV3ProjectsProjectIdCategoriesGet(projectId);
} catch (ApiException e) {
    System.err.println("Exception when calling CategoriesApi#apiV3ProjectsProjectIdCategoriesGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **projectId** | **Integer**| ID of the project whoose categories will be listed |

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

