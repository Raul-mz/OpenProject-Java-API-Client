
# WPTypes

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**total** | **Integer** |  |  [optional]
**count** | **Integer** |  |  [optional]
**embedded** | [**WPTypesEmbedded**](WPTypesEmbedded.md) |  |  [optional]
**links** | [**WPTypeLinks**](WPTypeLinks.md) |  |  [optional]



