
# VersionLinks

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**self** | [**Link**](Link.md) |  |  [optional]
**definingProject** | [**Link**](Link.md) |  |  [optional]
**availableInProjects** | [**Link**](Link.md) |  |  [optional]



