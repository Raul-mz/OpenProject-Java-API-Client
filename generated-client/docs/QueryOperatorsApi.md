# QueryOperatorsApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiV3QueriesOperatorsIdGet**](QueryOperatorsApi.md#apiV3QueriesOperatorsIdGet) | **GET** /api/v3/queries/operators/{id} | View Query Operator


<a name="apiV3QueriesOperatorsIdGet"></a>
# **apiV3QueriesOperatorsIdGet**
> apiV3QueriesOperatorsIdGet(id)

View Query Operator

Retreive an individual QueryOperator as identified by the &#x60;id&#x60; parameter.

### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.QueryOperatorsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

QueryOperatorsApi apiInstance = new QueryOperatorsApi();
String id = "id_example"; // String | QueryOperator id
try {
    apiInstance.apiV3QueriesOperatorsIdGet(id);
} catch (ApiException e) {
    System.err.println("Exception when calling QueryOperatorsApi#apiV3QueriesOperatorsIdGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| QueryOperator id |

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

