# WorkPackagesApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiV3ProjectsIdWorkPackagesFormPost**](WorkPackagesApi.md#apiV3ProjectsIdWorkPackagesFormPost) | **POST** /api/v3/projects/{id}/work_packages/form | Work Package Create Form
[**apiV3ProjectsIdWorkPackagesGet**](WorkPackagesApi.md#apiV3ProjectsIdWorkPackagesGet) | **GET** /api/v3/projects/{id}/work_packages | List Work Packages
[**apiV3ProjectsIdWorkPackagesPost**](WorkPackagesApi.md#apiV3ProjectsIdWorkPackagesPost) | **POST** /api/v3/projects/{id}/work_packages | Create Work Package
[**apiV3ProjectsProjectIdWorkPackagesAvailableAssigneesGet**](WorkPackagesApi.md#apiV3ProjectsProjectIdWorkPackagesAvailableAssigneesGet) | **GET** /api/v3/projects/{project_id}/work_packages/available_assignees | Available assignees
[**apiV3ProjectsProjectIdWorkPackagesAvailableResponsiblesGet**](WorkPackagesApi.md#apiV3ProjectsProjectIdWorkPackagesAvailableResponsiblesGet) | **GET** /api/v3/projects/{project_id}/work_packages/available_responsibles | Available responsibles
[**apiV3WorkPackagesFormPost**](WorkPackagesApi.md#apiV3WorkPackagesFormPost) | **POST** /api/v3/work_packages/form | Work Package Create Form
[**apiV3WorkPackagesGet**](WorkPackagesApi.md#apiV3WorkPackagesGet) | **GET** /api/v3/work_packages | List Work Packages
[**apiV3WorkPackagesIdActivitiesGet**](WorkPackagesApi.md#apiV3WorkPackagesIdActivitiesGet) | **GET** /api/v3/work_packages/{id}/activities | List work package activities
[**apiV3WorkPackagesIdActivitiesPost**](WorkPackagesApi.md#apiV3WorkPackagesIdActivitiesPost) | **POST** /api/v3/work_packages/{id}/activities | Comment work package
[**apiV3WorkPackagesIdAvailableProjectsGet**](WorkPackagesApi.md#apiV3WorkPackagesIdAvailableProjectsGet) | **GET** /api/v3/work_packages/{id}/available_projects | Available projects
[**apiV3WorkPackagesIdAvailableRelationCandidatesGet**](WorkPackagesApi.md#apiV3WorkPackagesIdAvailableRelationCandidatesGet) | **GET** /api/v3/work_packages/{id}/available_relation_candidates | Available relation candidates
[**apiV3WorkPackagesIdAvailableWatchersGet**](WorkPackagesApi.md#apiV3WorkPackagesIdAvailableWatchersGet) | **GET** /api/v3/work_packages/{id}/available_watchers | Available watchers
[**apiV3WorkPackagesIdDelete**](WorkPackagesApi.md#apiV3WorkPackagesIdDelete) | **DELETE** /api/v3/work_packages/{id} | Delete Work Package
[**apiV3WorkPackagesIdFormPost**](WorkPackagesApi.md#apiV3WorkPackagesIdFormPost) | **POST** /api/v3/work_packages/{id}/form | Work Package Edit Form
[**apiV3WorkPackagesIdGet**](WorkPackagesApi.md#apiV3WorkPackagesIdGet) | **GET** /api/v3/work_packages/{id} | View Work Package
[**apiV3WorkPackagesIdPatch**](WorkPackagesApi.md#apiV3WorkPackagesIdPatch) | **PATCH** /api/v3/work_packages/{id} | Edit Work Package
[**apiV3WorkPackagesIdRelationsFormPost**](WorkPackagesApi.md#apiV3WorkPackagesIdRelationsFormPost) | **POST** /api/v3/work_packages/{id}/relations/form | Relation create form
[**apiV3WorkPackagesIdRevisionsGet**](WorkPackagesApi.md#apiV3WorkPackagesIdRevisionsGet) | **GET** /api/v3/work_packages/{id}/revisions | Revisions
[**apiV3WorkPackagesPost**](WorkPackagesApi.md#apiV3WorkPackagesPost) | **POST** /api/v3/work_packages | Create Work Package
[**apiV3WorkPackagesSchemasGet**](WorkPackagesApi.md#apiV3WorkPackagesSchemasGet) | **GET** /api/v3/work_packages/schemas/ | List Work Package Schemas
[**apiV3WorkPackagesSchemasIdentifierGet**](WorkPackagesApi.md#apiV3WorkPackagesSchemasIdentifierGet) | **GET** /api/v3/work_packages/schemas/{identifier} | View Work Package Schema
[**apiV3WorkPackagesWorkPackageIdRelationsGet**](WorkPackagesApi.md#apiV3WorkPackagesWorkPackageIdRelationsGet) | **GET** /api/v3/work_packages/{work_package_id}/relations | List relations
[**apiV3WorkPackagesWorkPackageIdRelationsPost**](WorkPackagesApi.md#apiV3WorkPackagesWorkPackageIdRelationsPost) | **POST** /api/v3/work_packages/{work_package_id}/relations | Create Relation
[**apiV3WorkPackagesWorkPackageIdWatchersGet**](WorkPackagesApi.md#apiV3WorkPackagesWorkPackageIdWatchersGet) | **GET** /api/v3/work_packages/{work_package_id}/watchers | List watchers
[**apiV3WorkPackagesWorkPackageIdWatchersIdDelete**](WorkPackagesApi.md#apiV3WorkPackagesWorkPackageIdWatchersIdDelete) | **DELETE** /api/v3/work_packages/{work_package_id}/watchers/{id} | Remove watcher
[**apiV3WorkPackagesWorkPackageIdWatchersPost**](WorkPackagesApi.md#apiV3WorkPackagesWorkPackageIdWatchersPost) | **POST** /api/v3/work_packages/{work_package_id}/watchers | Add watcher


<a name="apiV3ProjectsIdWorkPackagesFormPost"></a>
# **apiV3ProjectsIdWorkPackagesFormPost**
> apiV3ProjectsIdWorkPackagesFormPost(id)

Work Package Create Form



### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.WorkPackagesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

WorkPackagesApi apiInstance = new WorkPackagesApi();
Integer id = 56; // Integer | ID of the project in which the work package will be created
try {
    apiInstance.apiV3ProjectsIdWorkPackagesFormPost(id);
} catch (ApiException e) {
    System.err.println("Exception when calling WorkPackagesApi#apiV3ProjectsIdWorkPackagesFormPost");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**| ID of the project in which the work package will be created |

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

<a name="apiV3ProjectsIdWorkPackagesGet"></a>
# **apiV3ProjectsIdWorkPackagesGet**
> WorkPackages apiV3ProjectsIdWorkPackagesGet(id, offset, pageSize, filters, sortBy, groupBy, showSums)

List Work Packages



### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.WorkPackagesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

WorkPackagesApi apiInstance = new WorkPackagesApi();
Integer id = 56; // Integer | Project id
Integer offset = 1; // Integer | Page number inside the requested collection.
Integer pageSize = 56; // Integer | Number of elements to display per page.
String filters = "filters_example"; // String | JSON specifying filter conditions. Accepts the same format as returned by the [queries](#queries) endpoint.
String sortBy = "sortBy_example"; // String | JSON specifying sort criteria. Accepts the same format as returned by the [queries](#queries) endpoint.
String groupBy = "groupBy_example"; // String | The column to group by.
Boolean showSums = false; // Boolean | Indicates whether properties should be summed up if they support it.
try {
    WorkPackages result = apiInstance.apiV3ProjectsIdWorkPackagesGet(id, offset, pageSize, filters, sortBy, groupBy, showSums);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling WorkPackagesApi#apiV3ProjectsIdWorkPackagesGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**| Project id |
 **offset** | **Integer**| Page number inside the requested collection. | [optional] [default to 1]
 **pageSize** | **Integer**| Number of elements to display per page. | [optional]
 **filters** | **String**| JSON specifying filter conditions. Accepts the same format as returned by the [queries](#queries) endpoint. | [optional]
 **sortBy** | **String**| JSON specifying sort criteria. Accepts the same format as returned by the [queries](#queries) endpoint. | [optional]
 **groupBy** | **String**| The column to group by. | [optional]
 **showSums** | **Boolean**| Indicates whether properties should be summed up if they support it. | [optional] [default to false]

### Return type

[**WorkPackages**](WorkPackages.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

<a name="apiV3ProjectsIdWorkPackagesPost"></a>
# **apiV3ProjectsIdWorkPackagesPost**
> WorkPackage apiV3ProjectsIdWorkPackagesPost(id, workPackage, notify)

Create Work Package

When calling this endpoint the client provides a single object, containing at least the properties and links that are required, in the body. The required fields of a WorkPackage can be found in its schema, which is embedded in the respective form. Note that it is only allowed to provide properties or links supporting the write operation.

### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.WorkPackagesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

WorkPackagesApi apiInstance = new WorkPackagesApi();
Integer id = 56; // Integer | Project id
WorkPackage workPackage = new WorkPackage(); // WorkPackage | Work package to add to the project
Boolean notify = true; // Boolean | Indicates whether change notifications (e.g. via E-Mail) should be sent. Note that this controls notifications for all users interested in changes to the work package (e.g. watchers, author and assignee), not just the current user.
try {
    WorkPackage result = apiInstance.apiV3ProjectsIdWorkPackagesPost(id, workPackage, notify);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling WorkPackagesApi#apiV3ProjectsIdWorkPackagesPost");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**| Project id |
 **workPackage** | [**WorkPackage**](WorkPackage.md)| Work package to add to the project |
 **notify** | **Boolean**| Indicates whether change notifications (e.g. via E-Mail) should be sent. Note that this controls notifications for all users interested in changes to the work package (e.g. watchers, author and assignee), not just the current user. | [optional] [default to true]

### Return type

[**WorkPackage**](WorkPackage.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

<a name="apiV3ProjectsProjectIdWorkPackagesAvailableAssigneesGet"></a>
# **apiV3ProjectsProjectIdWorkPackagesAvailableAssigneesGet**
> apiV3ProjectsProjectIdWorkPackagesAvailableAssigneesGet(projectId)

Available assignees

Gets a list of users that can be assigned to work packages in the given project.

### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.WorkPackagesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

WorkPackagesApi apiInstance = new WorkPackagesApi();
Integer projectId = 56; // Integer | Project id
try {
    apiInstance.apiV3ProjectsProjectIdWorkPackagesAvailableAssigneesGet(projectId);
} catch (ApiException e) {
    System.err.println("Exception when calling WorkPackagesApi#apiV3ProjectsProjectIdWorkPackagesAvailableAssigneesGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **projectId** | **Integer**| Project id |

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

<a name="apiV3ProjectsProjectIdWorkPackagesAvailableResponsiblesGet"></a>
# **apiV3ProjectsProjectIdWorkPackagesAvailableResponsiblesGet**
> apiV3ProjectsProjectIdWorkPackagesAvailableResponsiblesGet(projectId)

Available responsibles

Gets a list of users that can be assigned as the responsible of a work package in the given project.

### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.WorkPackagesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

WorkPackagesApi apiInstance = new WorkPackagesApi();
Integer projectId = 56; // Integer | Project id
try {
    apiInstance.apiV3ProjectsProjectIdWorkPackagesAvailableResponsiblesGet(projectId);
} catch (ApiException e) {
    System.err.println("Exception when calling WorkPackagesApi#apiV3ProjectsProjectIdWorkPackagesAvailableResponsiblesGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **projectId** | **Integer**| Project id |

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

<a name="apiV3WorkPackagesFormPost"></a>
# **apiV3WorkPackagesFormPost**
> apiV3WorkPackagesFormPost()

Work Package Create Form



### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.WorkPackagesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

WorkPackagesApi apiInstance = new WorkPackagesApi();
try {
    apiInstance.apiV3WorkPackagesFormPost();
} catch (ApiException e) {
    System.err.println("Exception when calling WorkPackagesApi#apiV3WorkPackagesFormPost");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

<a name="apiV3WorkPackagesGet"></a>
# **apiV3WorkPackagesGet**
> WorkPackages apiV3WorkPackagesGet(offset, pageSize, filters, sortBy, groupBy, showSums)

List Work Packages



### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.WorkPackagesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

WorkPackagesApi apiInstance = new WorkPackagesApi();
Integer offset = 1; // Integer | Page number inside the requested collection.
Integer pageSize = 56; // Integer | Number of elements to display per page.
String filters = "filters_example"; // String | JSON specifying filter conditions. Accepts the same format as returned by the [queries](#queries) endpoint.
String sortBy = "sortBy_example"; // String | JSON specifying sort criteria. Accepts the same format as returned by the [queries](#queries) endpoint.
String groupBy = "groupBy_example"; // String | The column to group by.
Boolean showSums = false; // Boolean | Indicates whether properties should be summed up if they support it.
try {
    WorkPackages result = apiInstance.apiV3WorkPackagesGet(offset, pageSize, filters, sortBy, groupBy, showSums);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling WorkPackagesApi#apiV3WorkPackagesGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **offset** | **Integer**| Page number inside the requested collection. | [optional] [default to 1]
 **pageSize** | **Integer**| Number of elements to display per page. | [optional]
 **filters** | **String**| JSON specifying filter conditions. Accepts the same format as returned by the [queries](#queries) endpoint. | [optional]
 **sortBy** | **String**| JSON specifying sort criteria. Accepts the same format as returned by the [queries](#queries) endpoint. | [optional]
 **groupBy** | **String**| The column to group by. | [optional]
 **showSums** | **Boolean**| Indicates whether properties should be summed up if they support it. | [optional] [default to false]

### Return type

[**WorkPackages**](WorkPackages.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

<a name="apiV3WorkPackagesIdActivitiesGet"></a>
# **apiV3WorkPackagesIdActivitiesGet**
> apiV3WorkPackagesIdActivitiesGet(id)

List work package activities



### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.WorkPackagesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

WorkPackagesApi apiInstance = new WorkPackagesApi();
Integer id = 56; // Integer | Work package id
try {
    apiInstance.apiV3WorkPackagesIdActivitiesGet(id);
} catch (ApiException e) {
    System.err.println("Exception when calling WorkPackagesApi#apiV3WorkPackagesIdActivitiesGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**| Work package id |

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

<a name="apiV3WorkPackagesIdActivitiesPost"></a>
# **apiV3WorkPackagesIdActivitiesPost**
> apiV3WorkPackagesIdActivitiesPost(id, notify, body)

Comment work package

Creates an activity for the selected work package and, on success, returns the updated activity.

### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.WorkPackagesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

WorkPackagesApi apiInstance = new WorkPackagesApi();
Integer id = 56; // Integer | Work package id
Boolean notify = true; // Boolean | Indicates whether change notifications (e.g. via E-Mail) should be sent. Note that this controls notifications for all users interested in changes to the work package (e.g. watchers, author and assignee), not just the current user.
Body8 body = new Body8(); // Body8 | 
try {
    apiInstance.apiV3WorkPackagesIdActivitiesPost(id, notify, body);
} catch (ApiException e) {
    System.err.println("Exception when calling WorkPackagesApi#apiV3WorkPackagesIdActivitiesPost");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**| Work package id |
 **notify** | **Boolean**| Indicates whether change notifications (e.g. via E-Mail) should be sent. Note that this controls notifications for all users interested in changes to the work package (e.g. watchers, author and assignee), not just the current user. | [optional] [default to true]
 **body** | [**Body8**](Body8.md)|  | [optional]

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

<a name="apiV3WorkPackagesIdAvailableProjectsGet"></a>
# **apiV3WorkPackagesIdAvailableProjectsGet**
> apiV3WorkPackagesIdAvailableProjectsGet(id)

Available projects

Gets a list of projects that are available as projects to which the work package can be moved.

### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.WorkPackagesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

WorkPackagesApi apiInstance = new WorkPackagesApi();
Integer id = 56; // Integer | work package id
try {
    apiInstance.apiV3WorkPackagesIdAvailableProjectsGet(id);
} catch (ApiException e) {
    System.err.println("Exception when calling WorkPackagesApi#apiV3WorkPackagesIdAvailableProjectsGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**| work package id |

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

<a name="apiV3WorkPackagesIdAvailableRelationCandidatesGet"></a>
# **apiV3WorkPackagesIdAvailableRelationCandidatesGet**
> apiV3WorkPackagesIdAvailableRelationCandidatesGet(id, pageSize, filters, query, type)

Available relation candidates



### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.WorkPackagesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

WorkPackagesApi apiInstance = new WorkPackagesApi();
Long id = 789L; // Long | ID of workpackage to return
Integer pageSize = 56; // Integer | Maximum number of candidates to list (default 10)
String filters = "filters_example"; // String | JSON specifying filter conditions. Accepts the same format as returned by the [queries](#queries) endpoint.
String query = "query_example"; // String | Shortcut for filtering by ID or subject
String type = "type_example"; // String | Type of relation to find candidates for (default \"relates\")
try {
    apiInstance.apiV3WorkPackagesIdAvailableRelationCandidatesGet(id, pageSize, filters, query, type);
} catch (ApiException e) {
    System.err.println("Exception when calling WorkPackagesApi#apiV3WorkPackagesIdAvailableRelationCandidatesGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**| ID of workpackage to return |
 **pageSize** | **Integer**| Maximum number of candidates to list (default 10) | [optional]
 **filters** | **String**| JSON specifying filter conditions. Accepts the same format as returned by the [queries](#queries) endpoint. | [optional]
 **query** | **String**| Shortcut for filtering by ID or subject | [optional]
 **type** | **String**| Type of relation to find candidates for (default \&quot;relates\&quot;) | [optional]

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

<a name="apiV3WorkPackagesIdAvailableWatchersGet"></a>
# **apiV3WorkPackagesIdAvailableWatchersGet**
> apiV3WorkPackagesIdAvailableWatchersGet(id)

Available watchers

Gets a list of users that are able to be watchers of the specified work package.

### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.WorkPackagesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

WorkPackagesApi apiInstance = new WorkPackagesApi();
Integer id = 56; // Integer | work package id
try {
    apiInstance.apiV3WorkPackagesIdAvailableWatchersGet(id);
} catch (ApiException e) {
    System.err.println("Exception when calling WorkPackagesApi#apiV3WorkPackagesIdAvailableWatchersGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**| work package id |

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

<a name="apiV3WorkPackagesIdDelete"></a>
# **apiV3WorkPackagesIdDelete**
> apiV3WorkPackagesIdDelete(id)

Delete Work Package

Deletes the work package, as well as:  * all associated time entries  * its hierarchy of child work packages

### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.WorkPackagesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

WorkPackagesApi apiInstance = new WorkPackagesApi();
Integer id = 56; // Integer | Work package id
try {
    apiInstance.apiV3WorkPackagesIdDelete(id);
} catch (ApiException e) {
    System.err.println("Exception when calling WorkPackagesApi#apiV3WorkPackagesIdDelete");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**| Work package id |

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

<a name="apiV3WorkPackagesIdFormPost"></a>
# **apiV3WorkPackagesIdFormPost**
> apiV3WorkPackagesIdFormPost(id)

Work Package Edit Form



### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.WorkPackagesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

WorkPackagesApi apiInstance = new WorkPackagesApi();
Integer id = 56; // Integer | ID of the work package being modified
try {
    apiInstance.apiV3WorkPackagesIdFormPost(id);
} catch (ApiException e) {
    System.err.println("Exception when calling WorkPackagesApi#apiV3WorkPackagesIdFormPost");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**| ID of the work package being modified |

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

<a name="apiV3WorkPackagesIdGet"></a>
# **apiV3WorkPackagesIdGet**
> WorkPackage apiV3WorkPackagesIdGet(id)

View Work Package



### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.WorkPackagesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

WorkPackagesApi apiInstance = new WorkPackagesApi();
Integer id = 56; // Integer | Work package id
try {
    WorkPackage result = apiInstance.apiV3WorkPackagesIdGet(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling WorkPackagesApi#apiV3WorkPackagesIdGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**| Work package id |

### Return type

[**WorkPackage**](WorkPackage.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

<a name="apiV3WorkPackagesIdPatch"></a>
# **apiV3WorkPackagesIdPatch**
> WorkPackage apiV3WorkPackagesIdPatch(id, notify, body)

Edit Work Package

When calling this endpoint the client provides a single object, containing the properties and links that it wants to change, in the body. Note that it is only allowed to provide properties or links supporting the **write** operation.  Additionally to the fields the client wants to change, it is mandatory to provide the value of &#x60;lockVersion&#x60; which was received by the &#x60;GET&#x60; request this change originates from.  The value of &#x60;lockVersion&#x60; is used to implement [optimistic locking](http://en.wikipedia.org/wiki/Optimistic_concurrency_control).

### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.WorkPackagesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

WorkPackagesApi apiInstance = new WorkPackagesApi();
Integer id = 56; // Integer | Work package id
Boolean notify = true; // Boolean | Indicates whether change notifications (e.g. via E-Mail) should be sent. Note that this controls notifications for all users interested in changes to the work package (e.g. watchers, author and assignee), not just the current user.
WorkPackage body = new WorkPackage(); // WorkPackage | 
try {
    WorkPackage result = apiInstance.apiV3WorkPackagesIdPatch(id, notify, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling WorkPackagesApi#apiV3WorkPackagesIdPatch");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**| Work package id |
 **notify** | **Boolean**| Indicates whether change notifications (e.g. via E-Mail) should be sent. Note that this controls notifications for all users interested in changes to the work package (e.g. watchers, author and assignee), not just the current user. | [optional] [default to true]
 **body** | [**WorkPackage**](WorkPackage.md)|  | [optional]

### Return type

[**WorkPackage**](WorkPackage.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

<a name="apiV3WorkPackagesIdRelationsFormPost"></a>
# **apiV3WorkPackagesIdRelationsFormPost**
> apiV3WorkPackagesIdRelationsFormPost(id)

Relation create form



### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.WorkPackagesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

WorkPackagesApi apiInstance = new WorkPackagesApi();
Integer id = 56; // Integer | ID of the relation being modified
try {
    apiInstance.apiV3WorkPackagesIdRelationsFormPost(id);
} catch (ApiException e) {
    System.err.println("Exception when calling WorkPackagesApi#apiV3WorkPackagesIdRelationsFormPost");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**| ID of the relation being modified |

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

<a name="apiV3WorkPackagesIdRevisionsGet"></a>
# **apiV3WorkPackagesIdRevisionsGet**
> apiV3WorkPackagesIdRevisionsGet(id)

Revisions

Gets a list of revisions that are linked to this work package, e.g., because it is referenced in the commit message of the revision. Only linked revisions from repositories are shown if the user has the view changesets permission in the defining project.

### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.WorkPackagesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

WorkPackagesApi apiInstance = new WorkPackagesApi();
Integer id = 56; // Integer | work package id
try {
    apiInstance.apiV3WorkPackagesIdRevisionsGet(id);
} catch (ApiException e) {
    System.err.println("Exception when calling WorkPackagesApi#apiV3WorkPackagesIdRevisionsGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**| work package id |

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

<a name="apiV3WorkPackagesPost"></a>
# **apiV3WorkPackagesPost**
> WorkPackage apiV3WorkPackagesPost(workPackage, notify)

Create Work Package

When calling this endpoint the client provides a single object, containing at least the properties and links that are required, in the body. The required fields of a WorkPackage can be found in its schema, which is embedded in the respective form. Note that it is only allowed to provide properties or links supporting the write operation.  A project link must be set when creating work packages through this route.

### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.WorkPackagesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

WorkPackagesApi apiInstance = new WorkPackagesApi();
WorkPackage workPackage = new WorkPackage(); // WorkPackage | Work package to add (a project link ist requried)
Boolean notify = true; // Boolean | Indicates whether change notifications (e.g. via E-Mail) should be sent. Note that this controls notifications for all users interested in changes to the work package (e.g. watchers, author and assignee), not just the current user.
try {
    WorkPackage result = apiInstance.apiV3WorkPackagesPost(workPackage, notify);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling WorkPackagesApi#apiV3WorkPackagesPost");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workPackage** | [**WorkPackage**](WorkPackage.md)| Work package to add (a project link ist requried) |
 **notify** | **Boolean**| Indicates whether change notifications (e.g. via E-Mail) should be sent. Note that this controls notifications for all users interested in changes to the work package (e.g. watchers, author and assignee), not just the current user. | [optional] [default to true]

### Return type

[**WorkPackage**](WorkPackage.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

<a name="apiV3WorkPackagesSchemasGet"></a>
# **apiV3WorkPackagesSchemasGet**
> apiV3WorkPackagesSchemasGet(filters)

List Work Package Schemas

List work package schemas.

### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.WorkPackagesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

WorkPackagesApi apiInstance = new WorkPackagesApi();
String filters = "filters_example"; // String | JSON specifying filter conditions. Accepts the same format as returned by the [queries](#queries) endpoint. Currently supported filters are:  + id: The schema's id
try {
    apiInstance.apiV3WorkPackagesSchemasGet(filters);
} catch (ApiException e) {
    System.err.println("Exception when calling WorkPackagesApi#apiV3WorkPackagesSchemasGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filters** | **String**| JSON specifying filter conditions. Accepts the same format as returned by the [queries](#queries) endpoint. Currently supported filters are:  + id: The schema&#39;s id |

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

<a name="apiV3WorkPackagesSchemasIdentifierGet"></a>
# **apiV3WorkPackagesSchemasIdentifierGet**
> apiV3WorkPackagesSchemasIdentifierGet(identifier)

View Work Package Schema



### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.WorkPackagesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

WorkPackagesApi apiInstance = new WorkPackagesApi();
String identifier = "identifier_example"; // String | Identifier of the schema
try {
    apiInstance.apiV3WorkPackagesSchemasIdentifierGet(identifier);
} catch (ApiException e) {
    System.err.println("Exception when calling WorkPackagesApi#apiV3WorkPackagesSchemasIdentifierGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **identifier** | **String**| Identifier of the schema |

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

<a name="apiV3WorkPackagesWorkPackageIdRelationsGet"></a>
# **apiV3WorkPackagesWorkPackageIdRelationsGet**
> apiV3WorkPackagesWorkPackageIdRelationsGet(workPackageId)

List relations

Lists all relations this work package is involved in.

### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.WorkPackagesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

WorkPackagesApi apiInstance = new WorkPackagesApi();
Integer workPackageId = 56; // Integer | Work package id
try {
    apiInstance.apiV3WorkPackagesWorkPackageIdRelationsGet(workPackageId);
} catch (ApiException e) {
    System.err.println("Exception when calling WorkPackagesApi#apiV3WorkPackagesWorkPackageIdRelationsGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workPackageId** | **Integer**| Work package id |

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain

<a name="apiV3WorkPackagesWorkPackageIdRelationsPost"></a>
# **apiV3WorkPackagesWorkPackageIdRelationsPost**
> apiV3WorkPackagesWorkPackageIdRelationsPost(workPackageId)

Create Relation

When calling this endpoint the client provides a single object, containing at least the properties and links that are required, in the body. The required fields of a Relation can be found in its schema, which is embedded in the respective form. Note that it is only allowed to provide properties or links supporting the write operation.

### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.WorkPackagesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

WorkPackagesApi apiInstance = new WorkPackagesApi();
Integer workPackageId = 56; // Integer | Work package id
try {
    apiInstance.apiV3WorkPackagesWorkPackageIdRelationsPost(workPackageId);
} catch (ApiException e) {
    System.err.println("Exception when calling WorkPackagesApi#apiV3WorkPackagesWorkPackageIdRelationsPost");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workPackageId** | **Integer**| Work package id |

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

<a name="apiV3WorkPackagesWorkPackageIdWatchersGet"></a>
# **apiV3WorkPackagesWorkPackageIdWatchersGet**
> apiV3WorkPackagesWorkPackageIdWatchersGet(workPackageId)

List watchers



### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.WorkPackagesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

WorkPackagesApi apiInstance = new WorkPackagesApi();
Integer workPackageId = 56; // Integer | Work package id
try {
    apiInstance.apiV3WorkPackagesWorkPackageIdWatchersGet(workPackageId);
} catch (ApiException e) {
    System.err.println("Exception when calling WorkPackagesApi#apiV3WorkPackagesWorkPackageIdWatchersGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workPackageId** | **Integer**| Work package id |

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

<a name="apiV3WorkPackagesWorkPackageIdWatchersIdDelete"></a>
# **apiV3WorkPackagesWorkPackageIdWatchersIdDelete**
> apiV3WorkPackagesWorkPackageIdWatchersIdDelete(workPackageId, id)

Remove watcher

Removes the specified user from the list of watchers for the given work package.  If the request succeeds, the specified user is not watching the work package anymore.  *Note: This might also be the case, if the specified user did not watch the work package prior to the request.*

### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.WorkPackagesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

WorkPackagesApi apiInstance = new WorkPackagesApi();
Integer workPackageId = 56; // Integer | Work package id
Integer id = 56; // Integer | User id
try {
    apiInstance.apiV3WorkPackagesWorkPackageIdWatchersIdDelete(workPackageId, id);
} catch (ApiException e) {
    System.err.println("Exception when calling WorkPackagesApi#apiV3WorkPackagesWorkPackageIdWatchersIdDelete");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workPackageId** | **Integer**| Work package id |
 **id** | **Integer**| User id |

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

<a name="apiV3WorkPackagesWorkPackageIdWatchersPost"></a>
# **apiV3WorkPackagesWorkPackageIdWatchersPost**
> apiV3WorkPackagesWorkPackageIdWatchersPost(workPackageId, body)

Add watcher

Adds a watcher to the specified work package.  The request is expected to contain a single JSON object, that contains a link object under the &#x60;user&#x60; key.  The response will be user added as watcher. In case the user was already watching the work package an &#x60;HTTP 200&#x60; is returned, an &#x60;HTTP 201&#x60; if the user was added as a new watcher.

### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.WorkPackagesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

WorkPackagesApi apiInstance = new WorkPackagesApi();
Integer workPackageId = 56; // Integer | Work package id
Body7 body = new Body7(); // Body7 | 
try {
    apiInstance.apiV3WorkPackagesWorkPackageIdWatchersPost(workPackageId, body);
} catch (ApiException e) {
    System.err.println("Exception when calling WorkPackagesApi#apiV3WorkPackagesWorkPackageIdWatchersPost");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workPackageId** | **Integer**| Work package id |
 **body** | [**Body7**](Body7.md)|  | [optional]

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

