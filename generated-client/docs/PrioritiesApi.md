# PrioritiesApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiV3PrioritiesGet**](PrioritiesApi.md#apiV3PrioritiesGet) | **GET** /api/v3/priorities | List all Priorities
[**apiV3PrioritiesIdGet**](PrioritiesApi.md#apiV3PrioritiesIdGet) | **GET** /api/v3/priorities/{id} | View Priority


<a name="apiV3PrioritiesGet"></a>
# **apiV3PrioritiesGet**
> apiV3PrioritiesGet()

List all Priorities



### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.PrioritiesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

PrioritiesApi apiInstance = new PrioritiesApi();
try {
    apiInstance.apiV3PrioritiesGet();
} catch (ApiException e) {
    System.err.println("Exception when calling PrioritiesApi#apiV3PrioritiesGet");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

<a name="apiV3PrioritiesIdGet"></a>
# **apiV3PrioritiesIdGet**
> apiV3PrioritiesIdGet(id)

View Priority



### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.PrioritiesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

PrioritiesApi apiInstance = new PrioritiesApi();
Integer id = 56; // Integer | Priority id
try {
    apiInstance.apiV3PrioritiesIdGet(id);
} catch (ApiException e) {
    System.err.println("Exception when calling PrioritiesApi#apiV3PrioritiesIdGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**| Priority id |

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

