# QueryColumnsApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiV3QueriesColumnsIdGet**](QueryColumnsApi.md#apiV3QueriesColumnsIdGet) | **GET** /api/v3/queries/columns/{id} | View Query Column


<a name="apiV3QueriesColumnsIdGet"></a>
# **apiV3QueriesColumnsIdGet**
> apiV3QueriesColumnsIdGet(id)

View Query Column

Retreive an individual QueryColumn as identified by the &#x60;id&#x60; parameter.

### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.QueryColumnsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

QueryColumnsApi apiInstance = new QueryColumnsApi();
String id = "id_example"; // String | QueryColumn id
try {
    apiInstance.apiV3QueriesColumnsIdGet(id);
} catch (ApiException e) {
    System.err.println("Exception when calling QueryColumnsApi#apiV3QueriesColumnsIdGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| QueryColumn id |

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

