# PreviewingApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiV3RenderPlainPost**](PreviewingApi.md#apiV3RenderPlainPost) | **POST** /api/v3/render/plain | Preview plain document
[**apiV3RenderTextilePost**](PreviewingApi.md#apiV3RenderTextilePost) | **POST** /api/v3/render/textile | Preview Textile document


<a name="apiV3RenderPlainPost"></a>
# **apiV3RenderPlainPost**
> apiV3RenderPlainPost()

Preview plain document



### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.PreviewingApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

PreviewingApi apiInstance = new PreviewingApi();
try {
    apiInstance.apiV3RenderPlainPost();
} catch (ApiException e) {
    System.err.println("Exception when calling PreviewingApi#apiV3RenderPlainPost");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/html, application/json

<a name="apiV3RenderTextilePost"></a>
# **apiV3RenderTextilePost**
> apiV3RenderTextilePost(context)

Preview Textile document



### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.PreviewingApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

PreviewingApi apiInstance = new PreviewingApi();
String context = "context_example"; // String | API-Link to the context in which the rendering occurs, for example a specific work package.  If left out only context-agnostic rendering takes place. Please note that OpenProject features textile-extensions that can only work given a context (e.g. display attached images).  **Supported contexts:**  * `/api/v3/work_packages/{id}` - an existing work package
try {
    apiInstance.apiV3RenderTextilePost(context);
} catch (ApiException e) {
    System.err.println("Exception when calling PreviewingApi#apiV3RenderTextilePost");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **context** | **String**| API-Link to the context in which the rendering occurs, for example a specific work package.  If left out only context-agnostic rendering takes place. Please note that OpenProject features textile-extensions that can only work given a context (e.g. display attached images).  **Supported contexts:**  * &#x60;/api/v3/work_packages/{id}&#x60; - an existing work package | [optional]

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/html, application/json

