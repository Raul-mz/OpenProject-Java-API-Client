# ConfigurationApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiV3ConfigurationGet**](ConfigurationApi.md#apiV3ConfigurationGet) | **GET** /api/v3/configuration | View configuration


<a name="apiV3ConfigurationGet"></a>
# **apiV3ConfigurationGet**
> apiV3ConfigurationGet()

View configuration



### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.ConfigurationApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

ConfigurationApi apiInstance = new ConfigurationApi();
try {
    apiInstance.apiV3ConfigurationGet();
} catch (ApiException e) {
    System.err.println("Exception when calling ConfigurationApi#apiV3ConfigurationGet");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

