
# WorkPackage

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  |  [optional]
**lockVersion** | **Integer** |  |  [optional]
**percentageDone** | **Integer** |  |  [optional]
**subject** | **String** |  |  [optional]
**description** | [**Description**](Description.md) |  |  [optional]
**startDate** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**dueDate** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**createdAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**updatedAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**estimatedTime** | **String** |  |  [optional]
**embedded** | [**WorkPackageEmbedded**](WorkPackageEmbedded.md) |  |  [optional]
**links** | [**WorkPackageLinks**](WorkPackageLinks.md) |  |  [optional]



