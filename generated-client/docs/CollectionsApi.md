# CollectionsApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiV3ExamplesGet**](CollectionsApi.md#apiV3ExamplesGet) | **GET** /api/v3/examples | view aggregated result


<a name="apiV3ExamplesGet"></a>
# **apiV3ExamplesGet**
> apiV3ExamplesGet(groupBy, showSums)

view aggregated result



### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.CollectionsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

CollectionsApi apiInstance = new CollectionsApi();
String groupBy = "groupBy_example"; // String | The column to group by. Note: Aggregation is as of now only supported by the work package collection. You can pass any column name as returned by the [queries](#queries) endpoint.
String showSums = "false"; // String | 
try {
    apiInstance.apiV3ExamplesGet(groupBy, showSums);
} catch (ApiException e) {
    System.err.println("Exception when calling CollectionsApi#apiV3ExamplesGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **groupBy** | **String**| The column to group by. Note: Aggregation is as of now only supported by the work package collection. You can pass any column name as returned by the [queries](#queries) endpoint. | [optional]
 **showSums** | **String**|  | [optional] [default to false]

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

