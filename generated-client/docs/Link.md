
# Link

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**href** | **String** |  |  [optional]
**title** | **String** |  |  [optional]
**method** | [**MethodEnum**](#MethodEnum) |  |  [optional]
**type** | **String** |  |  [optional]


<a name="MethodEnum"></a>
## Enum: MethodEnum
Name | Value
---- | -----
PATCH | &quot;patch&quot;
POST | &quot;post&quot;
GET | &quot;get&quot;



