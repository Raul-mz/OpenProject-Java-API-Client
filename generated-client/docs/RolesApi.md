# RolesApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiV3RolesGet**](RolesApi.md#apiV3RolesGet) | **GET** /api/v3/roles | List roles
[**apiV3RolesIdGet**](RolesApi.md#apiV3RolesIdGet) | **GET** /api/v3/roles/{id} | View role


<a name="apiV3RolesGet"></a>
# **apiV3RolesGet**
> apiV3RolesGet()

List roles

List all defined roles. This includes built in roles like &#39;Anonymous&#39; and &#39;Non member&#39;.

### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.RolesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

RolesApi apiInstance = new RolesApi();
try {
    apiInstance.apiV3RolesGet();
} catch (ApiException e) {
    System.err.println("Exception when calling RolesApi#apiV3RolesGet");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

<a name="apiV3RolesIdGet"></a>
# **apiV3RolesIdGet**
> apiV3RolesIdGet(id)

View role

Fetch an individual role.

### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.RolesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

RolesApi apiInstance = new RolesApi();
Integer id = 56; // Integer | role id
try {
    apiInstance.apiV3RolesIdGet(id);
} catch (ApiException e) {
    System.err.println("Exception when calling RolesApi#apiV3RolesIdGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**| role id |

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

