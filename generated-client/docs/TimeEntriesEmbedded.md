
# TimeEntriesEmbedded

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**elements** | [**List&lt;TimeEntry&gt;**](TimeEntry.md) |  |  [optional]



