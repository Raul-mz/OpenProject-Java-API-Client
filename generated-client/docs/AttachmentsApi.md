# AttachmentsApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiV3AttachmentsIdDelete**](AttachmentsApi.md#apiV3AttachmentsIdDelete) | **DELETE** /api/v3/attachments/{id} | Delete attachment
[**apiV3AttachmentsIdGet**](AttachmentsApi.md#apiV3AttachmentsIdGet) | **GET** /api/v3/attachments/{id} | View attachment
[**apiV3WorkPackagesIdAttachmentsGet**](AttachmentsApi.md#apiV3WorkPackagesIdAttachmentsGet) | **GET** /api/v3/work_packages/{id}/attachments | List attachments
[**apiV3WorkPackagesIdAttachmentsPost**](AttachmentsApi.md#apiV3WorkPackagesIdAttachmentsPost) | **POST** /api/v3/work_packages/{id}/attachments | Add attachment


<a name="apiV3AttachmentsIdDelete"></a>
# **apiV3AttachmentsIdDelete**
> apiV3AttachmentsIdDelete(id)

Delete attachment

Permanently deletes the specified attachment.

### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.AttachmentsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

AttachmentsApi apiInstance = new AttachmentsApi();
Integer id = 56; // Integer | Attachment id
try {
    apiInstance.apiV3AttachmentsIdDelete(id);
} catch (ApiException e) {
    System.err.println("Exception when calling AttachmentsApi#apiV3AttachmentsIdDelete");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**| Attachment id |

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

<a name="apiV3AttachmentsIdGet"></a>
# **apiV3AttachmentsIdGet**
> apiV3AttachmentsIdGet(id)

View attachment



### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.AttachmentsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

AttachmentsApi apiInstance = new AttachmentsApi();
Integer id = 56; // Integer | Attachment id
try {
    apiInstance.apiV3AttachmentsIdGet(id);
} catch (ApiException e) {
    System.err.println("Exception when calling AttachmentsApi#apiV3AttachmentsIdGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**| Attachment id |

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

<a name="apiV3WorkPackagesIdAttachmentsGet"></a>
# **apiV3WorkPackagesIdAttachmentsGet**
> apiV3WorkPackagesIdAttachmentsGet(id)

List attachments



### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.AttachmentsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

AttachmentsApi apiInstance = new AttachmentsApi();
Integer id = 56; // Integer | ID of the work package whose attachments will be listed
try {
    apiInstance.apiV3WorkPackagesIdAttachmentsGet(id);
} catch (ApiException e) {
    System.err.println("Exception when calling AttachmentsApi#apiV3WorkPackagesIdAttachmentsGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**| ID of the work package whose attachments will be listed |

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

<a name="apiV3WorkPackagesIdAttachmentsPost"></a>
# **apiV3WorkPackagesIdAttachmentsPost**
> apiV3WorkPackagesIdAttachmentsPost(id)

Add attachment

To add an attachment to a work package, a client needs to issue a request of type &#x60;multipart/form-data&#x60; with exactly two parts.  The first part *must* be called &#x60;metadata&#x60;. Its content type is expected to be &#x60;application/json&#x60;, the body *must* be a single JSON object, containing at least the &#x60;fileName&#x60; and optionally the attachments &#x60;description&#x60;.  The second part *must* be called &#x60;file&#x60;, its content type *should* match the mime type of the file. The body *must* be the raw content of the file. Note that a &#x60;filename&#x60; must be indicated in the &#x60;Content-Disposition&#x60; of this part, however it will be ignored. Instead the &#x60;fileName&#x60; inside the JSON of the metadata part will be used.

### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.AttachmentsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

AttachmentsApi apiInstance = new AttachmentsApi();
Integer id = 56; // Integer | ID of the work package to receive the attachment
try {
    apiInstance.apiV3WorkPackagesIdAttachmentsPost(id);
} catch (ApiException e) {
    System.err.println("Exception when calling AttachmentsApi#apiV3WorkPackagesIdAttachmentsPost");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**| ID of the work package to receive the attachment |

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

