
# TimeEntryEmbedded

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**project** | [**Project**](Project.md) |  |  [optional]
**workPackage** | [**WorkPackage**](WorkPackage.md) |  |  [optional]
**user** | [**User**](User.md) |  |  [optional]
**activity** | [**Activity**](Activity.md) |  |  [optional]



