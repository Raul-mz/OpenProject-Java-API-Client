# ActivitiesApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiV3ActivitiesIdGet**](ActivitiesApi.md#apiV3ActivitiesIdGet) | **GET** /api/v3/activities/{id} | View activity
[**apiV3ActivitiesIdPatch**](ActivitiesApi.md#apiV3ActivitiesIdPatch) | **PATCH** /api/v3/activities/{id} | Update activity


<a name="apiV3ActivitiesIdGet"></a>
# **apiV3ActivitiesIdGet**
> apiV3ActivitiesIdGet(id)

View activity



### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.ActivitiesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

ActivitiesApi apiInstance = new ActivitiesApi();
Integer id = 56; // Integer | Activity id
try {
    apiInstance.apiV3ActivitiesIdGet(id);
} catch (ApiException e) {
    System.err.println("Exception when calling ActivitiesApi#apiV3ActivitiesIdGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**| Activity id |

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

<a name="apiV3ActivitiesIdPatch"></a>
# **apiV3ActivitiesIdPatch**
> apiV3ActivitiesIdPatch(id, body)

Update activity

Updates an activity&#39;s comment and, on success, returns the updated activity.

### Example
```java
// Import classes:
//import org.openproject.client.ApiClient;
//import org.openproject.client.ApiException;
//import org.openproject.client.Configuration;
//import org.openproject.client.auth.*;
//import org.openproject.client.api.ActivitiesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

ActivitiesApi apiInstance = new ActivitiesApi();
Integer id = 56; // Integer | Activity id
Body body = new Body(); // Body | 
try {
    apiInstance.apiV3ActivitiesIdPatch(id, body);
} catch (ApiException e) {
    System.err.println("Exception when calling ActivitiesApi#apiV3ActivitiesIdPatch");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**| Activity id |
 **body** | [**Body**](Body.md)|  | [optional]

### Return type

null (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

