
# Body2

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lockVersion** | [**BigDecimal**](BigDecimal.md) |  |  [optional]
**type** | **String** |  |  [optional]
**subject** | **String** |  |  [optional]



