
# Projects

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**total** | **Integer** |  |  [optional]
**count** | **Integer** |  |  [optional]
**embedded** | [**ProjectsEmbedded**](ProjectsEmbedded.md) |  |  [optional]



