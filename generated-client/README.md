# openproject-api-client

## Requirements

Building the API client library requires [Maven](https://maven.apache.org/) to be installed.

## Installation

To install the API client library to your local Maven repository, simply execute:

```shell
mvn install
```

To deploy it to a remote Maven repository instead, configure the settings of the repository and execute:

```shell
mvn deploy
```

Refer to the [official documentation](https://maven.apache.org/plugins/maven-deploy-plugin/usage.html) for more information.

### Maven users

Add this dependency to your project's POM:

```xml
<dependency>
    <groupId>org.openproject.client</groupId>
    <artifactId>openproject-api-client</artifactId>
    <version>0.2.1</version>
    <scope>compile</scope>
</dependency>
```

### Gradle users

Add this dependency to your project's build file:

```groovy
compile "org.openproject.client:openproject-api-client:0.2.1"
```

### Others

At first generate the JAR by executing:

    mvn package

Then manually install the following JARs:

* target/openproject-api-client-0.2.1.jar
* target/lib/*.jar

## Getting Started

Please follow the [installation](#installation) instruction and execute the following Java code:

```java

import org.openproject.client.*;
import org.openproject.client.auth.*;
import org.openproject.client.model.*;
import org.openproject.client.api.ActivitiesApi;

import java.io.File;
import java.util.*;

public class ActivitiesApiExample {

    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        
        // Configure HTTP basic authorization: basicAuth
        HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
        basicAuth.setUsername("YOUR USERNAME");
        basicAuth.setPassword("YOUR PASSWORD");

        ActivitiesApi apiInstance = new ActivitiesApi();
        Integer id = 56; // Integer | Activity id
        try {
            apiInstance.apiV3ActivitiesIdGet(id);
        } catch (ApiException e) {
            System.err.println("Exception when calling ActivitiesApi#apiV3ActivitiesIdGet");
            e.printStackTrace();
        }
    }
}

```

## Documentation for API Endpoints

All URIs are relative to *https://localhost*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*ActivitiesApi* | [**apiV3ActivitiesIdGet**](docs/ActivitiesApi.md#apiV3ActivitiesIdGet) | **GET** /api/v3/activities/{id} | View activity
*ActivitiesApi* | [**apiV3ActivitiesIdPatch**](docs/ActivitiesApi.md#apiV3ActivitiesIdPatch) | **PATCH** /api/v3/activities/{id} | Update activity
*AttachmentsApi* | [**apiV3AttachmentsIdDelete**](docs/AttachmentsApi.md#apiV3AttachmentsIdDelete) | **DELETE** /api/v3/attachments/{id} | Delete attachment
*AttachmentsApi* | [**apiV3AttachmentsIdGet**](docs/AttachmentsApi.md#apiV3AttachmentsIdGet) | **GET** /api/v3/attachments/{id} | View attachment
*AttachmentsApi* | [**apiV3WorkPackagesIdAttachmentsGet**](docs/AttachmentsApi.md#apiV3WorkPackagesIdAttachmentsGet) | **GET** /api/v3/work_packages/{id}/attachments | List attachments
*AttachmentsApi* | [**apiV3WorkPackagesIdAttachmentsPost**](docs/AttachmentsApi.md#apiV3WorkPackagesIdAttachmentsPost) | **POST** /api/v3/work_packages/{id}/attachments | Add attachment
*CategoriesApi* | [**apiV3CategoriesIdGet**](docs/CategoriesApi.md#apiV3CategoriesIdGet) | **GET** /api/v3/categories/{id} | View Category
*CategoriesApi* | [**apiV3ProjectsProjectIdCategoriesGet**](docs/CategoriesApi.md#apiV3ProjectsProjectIdCategoriesGet) | **GET** /api/v3/projects/{project_id}/categories | List categories of a project
*CollectionsApi* | [**apiV3ExamplesGet**](docs/CollectionsApi.md#apiV3ExamplesGet) | **GET** /api/v3/examples | view aggregated result
*ConfigurationApi* | [**apiV3ConfigurationGet**](docs/ConfigurationApi.md#apiV3ConfigurationGet) | **GET** /api/v3/configuration | View configuration
*CustomActionsApi* | [**apiV3CustomActionsCustomActionIdExecutePost**](docs/CustomActionsApi.md#apiV3CustomActionsCustomActionIdExecutePost) | **POST** /api/v3/custom_actions/{custom_action_id}/execute | Execute custom action
*CustomActionsApi* | [**apiV3CustomActionsCustomActionIdGet**](docs/CustomActionsApi.md#apiV3CustomActionsCustomActionIdGet) | **GET** /api/v3/custom_actions/{custom_action_id} | View custom action
*CustomObjectsApi* | [**apiV3CustomObjectsIdGet**](docs/CustomObjectsApi.md#apiV3CustomObjectsIdGet) | **GET** /api/v3/custom_objects/{id} | View Custom Object
*FormsApi* | [**apiV3ExampleFormPost**](docs/FormsApi.md#apiV3ExampleFormPost) | **POST** /api/v3/example/form | show or validate form
*HelpTextsApi* | [**apiV3HelpTextsGet**](docs/HelpTextsApi.md#apiV3HelpTextsGet) | **GET** /api/v3/help_texts | List all help texts
*HelpTextsApi* | [**apiV3HelpTextsIdGet**](docs/HelpTextsApi.md#apiV3HelpTextsIdGet) | **GET** /api/v3/help_texts/{id} | View help text
*PreviewingApi* | [**apiV3RenderPlainPost**](docs/PreviewingApi.md#apiV3RenderPlainPost) | **POST** /api/v3/render/plain | Preview plain document
*PreviewingApi* | [**apiV3RenderTextilePost**](docs/PreviewingApi.md#apiV3RenderTextilePost) | **POST** /api/v3/render/textile | Preview Textile document
*PrincipalsApi* | [**apiV3PrincipalsGet**](docs/PrincipalsApi.md#apiV3PrincipalsGet) | **GET** /api/v3/principals | List principals
*PrioritiesApi* | [**apiV3PrioritiesGet**](docs/PrioritiesApi.md#apiV3PrioritiesGet) | **GET** /api/v3/priorities | List all Priorities
*PrioritiesApi* | [**apiV3PrioritiesIdGet**](docs/PrioritiesApi.md#apiV3PrioritiesIdGet) | **GET** /api/v3/priorities/{id} | View Priority
*ProjectsApi* | [**apiV3ProjectsGet**](docs/ProjectsApi.md#apiV3ProjectsGet) | **GET** /api/v3/projects | List projects
*ProjectsApi* | [**apiV3ProjectsIdGet**](docs/ProjectsApi.md#apiV3ProjectsIdGet) | **GET** /api/v3/projects/{id} | View project
*ProjectsApi* | [**apiV3VersionsIdProjectsGet**](docs/ProjectsApi.md#apiV3VersionsIdProjectsGet) | **GET** /api/v3/versions/{id}/projects | List projects with version
*QueriesApi* | [**apiV3ProjectsIdQueriesDefaultGet**](docs/QueriesApi.md#apiV3ProjectsIdQueriesDefaultGet) | **GET** /api/v3/projects/{id}/queries/default | View default query for project
*QueriesApi* | [**apiV3ProjectsIdQueriesSchemaGet**](docs/QueriesApi.md#apiV3ProjectsIdQueriesSchemaGet) | **GET** /api/v3/projects/{id}/queries/schema | View schema for project queries
*QueriesApi* | [**apiV3QueriesAvailableProjectsGet**](docs/QueriesApi.md#apiV3QueriesAvailableProjectsGet) | **GET** /api/v3/queries/available_projects | Available projects
*QueriesApi* | [**apiV3QueriesDefaultGet**](docs/QueriesApi.md#apiV3QueriesDefaultGet) | **GET** /api/v3/queries/default | View default query
*QueriesApi* | [**apiV3QueriesFormPost**](docs/QueriesApi.md#apiV3QueriesFormPost) | **POST** /api/v3/queries/form | Query Create Form
*QueriesApi* | [**apiV3QueriesGet**](docs/QueriesApi.md#apiV3QueriesGet) | **GET** /api/v3/queries | List queries
*QueriesApi* | [**apiV3QueriesIdDelete**](docs/QueriesApi.md#apiV3QueriesIdDelete) | **DELETE** /api/v3/queries/{id} | Delete query
*QueriesApi* | [**apiV3QueriesIdGet**](docs/QueriesApi.md#apiV3QueriesIdGet) | **GET** /api/v3/queries/{id} | View query
*QueriesApi* | [**apiV3QueriesIdPatch**](docs/QueriesApi.md#apiV3QueriesIdPatch) | **PATCH** /api/v3/queries/{id} | Edit Query
*QueriesApi* | [**apiV3QueriesIdStarPatch**](docs/QueriesApi.md#apiV3QueriesIdStarPatch) | **PATCH** /api/v3/queries/{id}/star | Star query
*QueriesApi* | [**apiV3QueriesIdUnstarPatch**](docs/QueriesApi.md#apiV3QueriesIdUnstarPatch) | **PATCH** /api/v3/queries/{id}/unstar | Unstar query
*QueriesApi* | [**apiV3QueriesPost**](docs/QueriesApi.md#apiV3QueriesPost) | **POST** /api/v3/queries | Create query
*QueriesApi* | [**apiV3QueriesSchemaGet**](docs/QueriesApi.md#apiV3QueriesSchemaGet) | **GET** /api/v3/queries/schema | View schema for global queries
*QueryColumnsApi* | [**apiV3QueriesColumnsIdGet**](docs/QueryColumnsApi.md#apiV3QueriesColumnsIdGet) | **GET** /api/v3/queries/columns/{id} | View Query Column
*QueryFilterInstanceSchemaApi* | [**apiV3ProjectsIdQueriesFilterInstanceSchemasGet**](docs/QueryFilterInstanceSchemaApi.md#apiV3ProjectsIdQueriesFilterInstanceSchemasGet) | **GET** /api/v3/projects/{id}/queries/filter_instance_schemas | List Query Filter Instance Schemas for Project
*QueryFilterInstanceSchemaApi* | [**apiV3QueriesFilterInstanceSchemasGet**](docs/QueryFilterInstanceSchemaApi.md#apiV3QueriesFilterInstanceSchemasGet) | **GET** /api/v3/queries/filter_instance_schemas | List Query Filter Instance Schemas
*QueryFilterInstanceSchemaApi* | [**apiV3QueriesFilterInstanceSchemasIdentifierGet**](docs/QueryFilterInstanceSchemaApi.md#apiV3QueriesFilterInstanceSchemasIdentifierGet) | **GET** /api/v3/queries/filter_instance_schemas/{identifier} | View Query Filter Instance Schema
*QueryFiltersApi* | [**apiV3QueriesFiltersIdGet**](docs/QueryFiltersApi.md#apiV3QueriesFiltersIdGet) | **GET** /api/v3/queries/filters/{id} | View Query Filter
*QueryOperatorsApi* | [**apiV3QueriesOperatorsIdGet**](docs/QueryOperatorsApi.md#apiV3QueriesOperatorsIdGet) | **GET** /api/v3/queries/operators/{id} | View Query Operator
*QuerySortBysApi* | [**apiV3QueriesSortBysIdGet**](docs/QuerySortBysApi.md#apiV3QueriesSortBysIdGet) | **GET** /api/v3/queries/sort_bys/{id} | View Query Sort By
*RelationsApi* | [**apiV3RelationsGet**](docs/RelationsApi.md#apiV3RelationsGet) | **GET** /api/v3/relations | List Relations
*RelationsApi* | [**apiV3RelationsIdDelete**](docs/RelationsApi.md#apiV3RelationsIdDelete) | **DELETE** /api/v3/relations/{id} | Delete Relation
*RelationsApi* | [**apiV3RelationsIdFormPost**](docs/RelationsApi.md#apiV3RelationsIdFormPost) | **POST** /api/v3/relations/{id}/form | Relation edit form
*RelationsApi* | [**apiV3RelationsIdGet**](docs/RelationsApi.md#apiV3RelationsIdGet) | **GET** /api/v3/relations/{id} | View Relation
*RelationsApi* | [**apiV3RelationsIdPatch**](docs/RelationsApi.md#apiV3RelationsIdPatch) | **PATCH** /api/v3/relations/{id} | Edit Relation
*RelationsApi* | [**apiV3RelationsSchemaGet**](docs/RelationsApi.md#apiV3RelationsSchemaGet) | **GET** /api/v3/relations/schema | View relation schema
*RelationsApi* | [**apiV3RelationsSchemaTypeGet**](docs/RelationsApi.md#apiV3RelationsSchemaTypeGet) | **GET** /api/v3/relations/schema/{type} | View relation schema for type
*RevisionsApi* | [**apiV3RevisionsIdGet**](docs/RevisionsApi.md#apiV3RevisionsIdGet) | **GET** /api/v3/revisions/{id} | View revision
*RolesApi* | [**apiV3RolesGet**](docs/RolesApi.md#apiV3RolesGet) | **GET** /api/v3/roles | List roles
*RolesApi* | [**apiV3RolesIdGet**](docs/RolesApi.md#apiV3RolesIdGet) | **GET** /api/v3/roles/{id} | View role
*RootApi* | [**apiV3Get**](docs/RootApi.md#apiV3Get) | **GET** /api/v3 | View root
*SchemasApi* | [**apiV3ExampleSchemaGet**](docs/SchemasApi.md#apiV3ExampleSchemaGet) | **GET** /api/v3/example/schema | view the schema
*StatusesApi* | [**apiV3StatusesGet**](docs/StatusesApi.md#apiV3StatusesGet) | **GET** /api/v3/statuses | List all Statuses
*StatusesApi* | [**apiV3StatusesIdGet**](docs/StatusesApi.md#apiV3StatusesIdGet) | **GET** /api/v3/statuses/{id} | View Status
*StringObjectsApi* | [**apiV3StringObjectsGet**](docs/StringObjectsApi.md#apiV3StringObjectsGet) | **GET** /api/v3/string_objects | View String Object
*TimeEntriesApi* | [**apiV3TimeEntriesGet**](docs/TimeEntriesApi.md#apiV3TimeEntriesGet) | **GET** /api/v3/time_entries | List Time entries
*TimeEntriesApi* | [**apiV3TimeEntriesIdGet**](docs/TimeEntriesApi.md#apiV3TimeEntriesIdGet) | **GET** /api/v3/time_entries/{id} | View time entry
*TimeEntryActivitiesApi* | [**apiV3TimeEntriesActivityIdGet**](docs/TimeEntryActivitiesApi.md#apiV3TimeEntriesActivityIdGet) | **GET** /api/v3/time_entries/activity/{id} | View time entries activity
*TypesApi* | [**apiV3ProjectsProjectIdTypesGet**](docs/TypesApi.md#apiV3ProjectsProjectIdTypesGet) | **GET** /api/v3/projects/{project_id}/types | List types available in a project
*TypesApi* | [**apiV3TypesGet**](docs/TypesApi.md#apiV3TypesGet) | **GET** /api/v3/types | List all Types
*TypesApi* | [**apiV3TypesIdGet**](docs/TypesApi.md#apiV3TypesIdGet) | **GET** /api/v3/types/{id} | View Type
*UserPreferencesApi* | [**apiV3MyPreferencesGet**](docs/UserPreferencesApi.md#apiV3MyPreferencesGet) | **GET** /api/v3/my_preferences | Show my preferences
*UserPreferencesApi* | [**apiV3MyPreferencesPatch**](docs/UserPreferencesApi.md#apiV3MyPreferencesPatch) | **PATCH** /api/v3/my_preferences | Update UserPreferences
*UsersApi* | [**apiV3UsersGet**](docs/UsersApi.md#apiV3UsersGet) | **GET** /api/v3/users | List Users
*UsersApi* | [**apiV3UsersIdDelete**](docs/UsersApi.md#apiV3UsersIdDelete) | **DELETE** /api/v3/users/{id} | Delete user
*UsersApi* | [**apiV3UsersIdGet**](docs/UsersApi.md#apiV3UsersIdGet) | **GET** /api/v3/users/{id} | View user
*UsersApi* | [**apiV3UsersIdLockDelete**](docs/UsersApi.md#apiV3UsersIdLockDelete) | **DELETE** /api/v3/users/{id}/lock | Remove Lock
*UsersApi* | [**apiV3UsersIdLockPost**](docs/UsersApi.md#apiV3UsersIdLockPost) | **POST** /api/v3/users/{id}/lock | Set Lock
*UsersApi* | [**apiV3UsersIdPatch**](docs/UsersApi.md#apiV3UsersIdPatch) | **PATCH** /api/v3/users/{id} | Update user
*UsersApi* | [**apiV3UsersPost**](docs/UsersApi.md#apiV3UsersPost) | **POST** /api/v3/users | Create User
*VersionsApi* | [**apiV3ProjectsProjectIdVersionsGet**](docs/VersionsApi.md#apiV3ProjectsProjectIdVersionsGet) | **GET** /api/v3/projects/{project_id}/versions | List versions available in a project
*VersionsApi* | [**apiV3VersionsGet**](docs/VersionsApi.md#apiV3VersionsGet) | **GET** /api/v3/versions | List versions
*VersionsApi* | [**apiV3VersionsIdGet**](docs/VersionsApi.md#apiV3VersionsIdGet) | **GET** /api/v3/versions/{id} | View version
*WorkPackagesApi* | [**apiV3ProjectsIdWorkPackagesFormPost**](docs/WorkPackagesApi.md#apiV3ProjectsIdWorkPackagesFormPost) | **POST** /api/v3/projects/{id}/work_packages/form | Work Package Create Form
*WorkPackagesApi* | [**apiV3ProjectsIdWorkPackagesGet**](docs/WorkPackagesApi.md#apiV3ProjectsIdWorkPackagesGet) | **GET** /api/v3/projects/{id}/work_packages | List Work Packages
*WorkPackagesApi* | [**apiV3ProjectsIdWorkPackagesPost**](docs/WorkPackagesApi.md#apiV3ProjectsIdWorkPackagesPost) | **POST** /api/v3/projects/{id}/work_packages | Create Work Package
*WorkPackagesApi* | [**apiV3ProjectsProjectIdWorkPackagesAvailableAssigneesGet**](docs/WorkPackagesApi.md#apiV3ProjectsProjectIdWorkPackagesAvailableAssigneesGet) | **GET** /api/v3/projects/{project_id}/work_packages/available_assignees | Available assignees
*WorkPackagesApi* | [**apiV3ProjectsProjectIdWorkPackagesAvailableResponsiblesGet**](docs/WorkPackagesApi.md#apiV3ProjectsProjectIdWorkPackagesAvailableResponsiblesGet) | **GET** /api/v3/projects/{project_id}/work_packages/available_responsibles | Available responsibles
*WorkPackagesApi* | [**apiV3WorkPackagesFormPost**](docs/WorkPackagesApi.md#apiV3WorkPackagesFormPost) | **POST** /api/v3/work_packages/form | Work Package Create Form
*WorkPackagesApi* | [**apiV3WorkPackagesGet**](docs/WorkPackagesApi.md#apiV3WorkPackagesGet) | **GET** /api/v3/work_packages | List Work Packages
*WorkPackagesApi* | [**apiV3WorkPackagesIdActivitiesGet**](docs/WorkPackagesApi.md#apiV3WorkPackagesIdActivitiesGet) | **GET** /api/v3/work_packages/{id}/activities | List work package activities
*WorkPackagesApi* | [**apiV3WorkPackagesIdActivitiesPost**](docs/WorkPackagesApi.md#apiV3WorkPackagesIdActivitiesPost) | **POST** /api/v3/work_packages/{id}/activities | Comment work package
*WorkPackagesApi* | [**apiV3WorkPackagesIdAvailableProjectsGet**](docs/WorkPackagesApi.md#apiV3WorkPackagesIdAvailableProjectsGet) | **GET** /api/v3/work_packages/{id}/available_projects | Available projects
*WorkPackagesApi* | [**apiV3WorkPackagesIdAvailableRelationCandidatesGet**](docs/WorkPackagesApi.md#apiV3WorkPackagesIdAvailableRelationCandidatesGet) | **GET** /api/v3/work_packages/{id}/available_relation_candidates | Available relation candidates
*WorkPackagesApi* | [**apiV3WorkPackagesIdAvailableWatchersGet**](docs/WorkPackagesApi.md#apiV3WorkPackagesIdAvailableWatchersGet) | **GET** /api/v3/work_packages/{id}/available_watchers | Available watchers
*WorkPackagesApi* | [**apiV3WorkPackagesIdDelete**](docs/WorkPackagesApi.md#apiV3WorkPackagesIdDelete) | **DELETE** /api/v3/work_packages/{id} | Delete Work Package
*WorkPackagesApi* | [**apiV3WorkPackagesIdFormPost**](docs/WorkPackagesApi.md#apiV3WorkPackagesIdFormPost) | **POST** /api/v3/work_packages/{id}/form | Work Package Edit Form
*WorkPackagesApi* | [**apiV3WorkPackagesIdGet**](docs/WorkPackagesApi.md#apiV3WorkPackagesIdGet) | **GET** /api/v3/work_packages/{id} | View Work Package
*WorkPackagesApi* | [**apiV3WorkPackagesIdPatch**](docs/WorkPackagesApi.md#apiV3WorkPackagesIdPatch) | **PATCH** /api/v3/work_packages/{id} | Edit Work Package
*WorkPackagesApi* | [**apiV3WorkPackagesIdRelationsFormPost**](docs/WorkPackagesApi.md#apiV3WorkPackagesIdRelationsFormPost) | **POST** /api/v3/work_packages/{id}/relations/form | Relation create form
*WorkPackagesApi* | [**apiV3WorkPackagesIdRevisionsGet**](docs/WorkPackagesApi.md#apiV3WorkPackagesIdRevisionsGet) | **GET** /api/v3/work_packages/{id}/revisions | Revisions
*WorkPackagesApi* | [**apiV3WorkPackagesPost**](docs/WorkPackagesApi.md#apiV3WorkPackagesPost) | **POST** /api/v3/work_packages | Create Work Package
*WorkPackagesApi* | [**apiV3WorkPackagesSchemasGet**](docs/WorkPackagesApi.md#apiV3WorkPackagesSchemasGet) | **GET** /api/v3/work_packages/schemas/ | List Work Package Schemas
*WorkPackagesApi* | [**apiV3WorkPackagesSchemasIdentifierGet**](docs/WorkPackagesApi.md#apiV3WorkPackagesSchemasIdentifierGet) | **GET** /api/v3/work_packages/schemas/{identifier} | View Work Package Schema
*WorkPackagesApi* | [**apiV3WorkPackagesWorkPackageIdRelationsGet**](docs/WorkPackagesApi.md#apiV3WorkPackagesWorkPackageIdRelationsGet) | **GET** /api/v3/work_packages/{work_package_id}/relations | List relations
*WorkPackagesApi* | [**apiV3WorkPackagesWorkPackageIdRelationsPost**](docs/WorkPackagesApi.md#apiV3WorkPackagesWorkPackageIdRelationsPost) | **POST** /api/v3/work_packages/{work_package_id}/relations | Create Relation
*WorkPackagesApi* | [**apiV3WorkPackagesWorkPackageIdWatchersGet**](docs/WorkPackagesApi.md#apiV3WorkPackagesWorkPackageIdWatchersGet) | **GET** /api/v3/work_packages/{work_package_id}/watchers | List watchers
*WorkPackagesApi* | [**apiV3WorkPackagesWorkPackageIdWatchersIdDelete**](docs/WorkPackagesApi.md#apiV3WorkPackagesWorkPackageIdWatchersIdDelete) | **DELETE** /api/v3/work_packages/{work_package_id}/watchers/{id} | Remove watcher
*WorkPackagesApi* | [**apiV3WorkPackagesWorkPackageIdWatchersPost**](docs/WorkPackagesApi.md#apiV3WorkPackagesWorkPackageIdWatchersPost) | **POST** /api/v3/work_packages/{work_package_id}/watchers | Add watcher


## Documentation for Models

 - [Activity](docs/Activity.md)
 - [ActivityModel](docs/ActivityModel.md)
 - [Aggregations](docs/Aggregations.md)
 - [Apiv3activitiesidComment](docs/Apiv3activitiesidComment.md)
 - [Apiv3customActionscustomActionIdexecuteLinks](docs/Apiv3customActionscustomActionIdexecuteLinks.md)
 - [Apiv3customActionscustomActionIdexecuteLinksWorkPackage](docs/Apiv3customActionscustomActionIdexecuteLinksWorkPackage.md)
 - [Attachment](docs/Attachment.md)
 - [AttachmentModel](docs/AttachmentModel.md)
 - [AttachmentsByWorkPackage](docs/AttachmentsByWorkPackage.md)
 - [AttachmentsByWorkPackageModel](docs/AttachmentsByWorkPackageModel.md)
 - [AvailableAssignees](docs/AvailableAssignees.md)
 - [AvailableAssigneesModel](docs/AvailableAssigneesModel.md)
 - [AvailableProjects](docs/AvailableProjects.md)
 - [AvailableProjectsModel](docs/AvailableProjectsModel.md)
 - [AvailableRelationCandidates](docs/AvailableRelationCandidates.md)
 - [AvailableRelationCandidatesModel](docs/AvailableRelationCandidatesModel.md)
 - [AvailableResponsibles](docs/AvailableResponsibles.md)
 - [AvailableResponsiblesModel](docs/AvailableResponsiblesModel.md)
 - [AvailableWatchers](docs/AvailableWatchers.md)
 - [AvailableWatchersModel](docs/AvailableWatchersModel.md)
 - [Body](docs/Body.md)
 - [Body1](docs/Body1.md)
 - [Body2](docs/Body2.md)
 - [Body3](docs/Body3.md)
 - [Body4](docs/Body4.md)
 - [Body5](docs/Body5.md)
 - [Body6](docs/Body6.md)
 - [Body7](docs/Body7.md)
 - [Body8](docs/Body8.md)
 - [CategoriesByProject](docs/CategoriesByProject.md)
 - [CategoriesByProjectModel](docs/CategoriesByProjectModel.md)
 - [Category](docs/Category.md)
 - [CategoryModel](docs/CategoryModel.md)
 - [ConfigurationModel](docs/ConfigurationModel.md)
 - [CursorBasedPagination](docs/CursorBasedPagination.md)
 - [CustomAction](docs/CustomAction.md)
 - [CustomActionModel](docs/CustomActionModel.md)
 - [CustomObject](docs/CustomObject.md)
 - [CustomObjectModel](docs/CustomObjectModel.md)
 - [DefaultQuery](docs/DefaultQuery.md)
 - [DefaultQueryForProject](docs/DefaultQueryForProject.md)
 - [DefaultQueryForProjectModel](docs/DefaultQueryForProjectModel.md)
 - [DefaultQueryModel](docs/DefaultQueryModel.md)
 - [Description](docs/Description.md)
 - [ExampleForm](docs/ExampleForm.md)
 - [ExampleFormModel](docs/ExampleFormModel.md)
 - [ExampleSchema](docs/ExampleSchema.md)
 - [ExampleSchemaModel](docs/ExampleSchemaModel.md)
 - [ExecuteCustomAction](docs/ExecuteCustomAction.md)
 - [HelpText](docs/HelpText.md)
 - [HelpTextModel](docs/HelpTextModel.md)
 - [HelpTexts](docs/HelpTexts.md)
 - [HelpTextsModel](docs/HelpTextsModel.md)
 - [Link](docs/Link.md)
 - [ModelConfiguration](docs/ModelConfiguration.md)
 - [OffsetBasedPagination](docs/OffsetBasedPagination.md)
 - [PlainText](docs/PlainText.md)
 - [PlainTextModel](docs/PlainTextModel.md)
 - [Principals](docs/Principals.md)
 - [PrincipalsModel](docs/PrincipalsModel.md)
 - [Priorities](docs/Priorities.md)
 - [PrioritiesModel](docs/PrioritiesModel.md)
 - [Priority](docs/Priority.md)
 - [PriorityModel](docs/PriorityModel.md)
 - [Project](docs/Project.md)
 - [ProjectModel](docs/ProjectModel.md)
 - [Projects](docs/Projects.md)
 - [ProjectsByVersion](docs/ProjectsByVersion.md)
 - [ProjectsByVersionModel](docs/ProjectsByVersionModel.md)
 - [ProjectsEmbedded](docs/ProjectsEmbedded.md)
 - [ProjectsModel](docs/ProjectsModel.md)
 - [Queries](docs/Queries.md)
 - [QueriesModel](docs/QueriesModel.md)
 - [Query](docs/Query.md)
 - [QueryAvailableProjects](docs/QueryAvailableProjects.md)
 - [QueryAvailableProjectsModel](docs/QueryAvailableProjectsModel.md)
 - [QueryColumn](docs/QueryColumn.md)
 - [QueryColumnModel](docs/QueryColumnModel.md)
 - [QueryCreateForm](docs/QueryCreateForm.md)
 - [QueryFilter](docs/QueryFilter.md)
 - [QueryFilterInstanceSchema](docs/QueryFilterInstanceSchema.md)
 - [QueryFilterInstanceSchemaModel](docs/QueryFilterInstanceSchemaModel.md)
 - [QueryFilterInstanceSchemas](docs/QueryFilterInstanceSchemas.md)
 - [QueryFilterInstanceSchemasForProject](docs/QueryFilterInstanceSchemasForProject.md)
 - [QueryFilterInstanceSchemasForProjectModel](docs/QueryFilterInstanceSchemasForProjectModel.md)
 - [QueryFilterInstanceSchemasModel](docs/QueryFilterInstanceSchemasModel.md)
 - [QueryFilterModel](docs/QueryFilterModel.md)
 - [QueryModel](docs/QueryModel.md)
 - [QueryOperator](docs/QueryOperator.md)
 - [QueryOperatorModel](docs/QueryOperatorModel.md)
 - [QuerySortBy](docs/QuerySortBy.md)
 - [QuerySortByModel](docs/QuerySortByModel.md)
 - [Relation](docs/Relation.md)
 - [RelationEditForm](docs/RelationEditForm.md)
 - [RelationEditFormModel](docs/RelationEditFormModel.md)
 - [RelationModel](docs/RelationModel.md)
 - [RelationSchema](docs/RelationSchema.md)
 - [RelationSchemaForType](docs/RelationSchemaForType.md)
 - [RelationSchemaModel](docs/RelationSchemaModel.md)
 - [Relations](docs/Relations.md)
 - [RelationsModel](docs/RelationsModel.md)
 - [RemoveWatcher](docs/RemoveWatcher.md)
 - [Revision](docs/Revision.md)
 - [RevisionModel](docs/RevisionModel.md)
 - [Revisions](docs/Revisions.md)
 - [RevisionsModel](docs/RevisionsModel.md)
 - [Role](docs/Role.md)
 - [RoleModel](docs/RoleModel.md)
 - [Roles](docs/Roles.md)
 - [RolesModel](docs/RolesModel.md)
 - [Root](docs/Root.md)
 - [RootModel](docs/RootModel.md)
 - [SchemaForGlobalQueries](docs/SchemaForGlobalQueries.md)
 - [SchemaForGlobalQueriesModel](docs/SchemaForGlobalQueriesModel.md)
 - [SchemaForProjectQueries](docs/SchemaForProjectQueries.md)
 - [SchemaForProjectQueriesModel](docs/SchemaForProjectQueriesModel.md)
 - [StarQuery](docs/StarQuery.md)
 - [StarQueryModel](docs/StarQueryModel.md)
 - [Status](docs/Status.md)
 - [StatusModel](docs/StatusModel.md)
 - [Statuses](docs/Statuses.md)
 - [StatusesModel](docs/StatusesModel.md)
 - [StringObject](docs/StringObject.md)
 - [StringObjectModel](docs/StringObjectModel.md)
 - [Textile](docs/Textile.md)
 - [TextileModel](docs/TextileModel.md)
 - [TimeEntries](docs/TimeEntries.md)
 - [TimeEntriesActivity](docs/TimeEntriesActivity.md)
 - [TimeEntriesActivityModel](docs/TimeEntriesActivityModel.md)
 - [TimeEntriesEmbedded](docs/TimeEntriesEmbedded.md)
 - [TimeEntriesLinks](docs/TimeEntriesLinks.md)
 - [TimeEntriesModel](docs/TimeEntriesModel.md)
 - [TimeEntry](docs/TimeEntry.md)
 - [TimeEntryEmbedded](docs/TimeEntryEmbedded.md)
 - [TimeEntryLinks](docs/TimeEntryLinks.md)
 - [TimeEntryModel](docs/TimeEntryModel.md)
 - [TypeModel](docs/TypeModel.md)
 - [TypesByProject](docs/TypesByProject.md)
 - [TypesByProjectModel](docs/TypesByProjectModel.md)
 - [TypesModel](docs/TypesModel.md)
 - [UnstarQuery](docs/UnstarQuery.md)
 - [UnstarQueryModel](docs/UnstarQueryModel.md)
 - [User](docs/User.md)
 - [UserAccountLocking](docs/UserAccountLocking.md)
 - [UserModel](docs/UserModel.md)
 - [UserPreferences](docs/UserPreferences.md)
 - [UserPreferencesModel](docs/UserPreferencesModel.md)
 - [Users](docs/Users.md)
 - [UsersEmbedded](docs/UsersEmbedded.md)
 - [UsersModel](docs/UsersModel.md)
 - [Version](docs/Version.md)
 - [VersionLinks](docs/VersionLinks.md)
 - [VersionModel](docs/VersionModel.md)
 - [Versions](docs/Versions.md)
 - [VersionsByProject](docs/VersionsByProject.md)
 - [VersionsByProjectModel](docs/VersionsByProjectModel.md)
 - [VersionsEmbedded](docs/VersionsEmbedded.md)
 - [VersionsModel](docs/VersionsModel.md)
 - [WPType](docs/WPType.md)
 - [WPTypeLinks](docs/WPTypeLinks.md)
 - [WPTypes](docs/WPTypes.md)
 - [WPTypesEmbedded](docs/WPTypesEmbedded.md)
 - [Watchers](docs/Watchers.md)
 - [WatchersModel](docs/WatchersModel.md)
 - [WorkPackage](docs/WorkPackage.md)
 - [WorkPackageActivities](docs/WorkPackageActivities.md)
 - [WorkPackageActivitiesModel](docs/WorkPackageActivitiesModel.md)
 - [WorkPackageCreateForm](docs/WorkPackageCreateForm.md)
 - [WorkPackageEditForm](docs/WorkPackageEditForm.md)
 - [WorkPackageEmbedded](docs/WorkPackageEmbedded.md)
 - [WorkPackageLinks](docs/WorkPackageLinks.md)
 - [WorkPackageModel](docs/WorkPackageModel.md)
 - [WorkPackagePatch](docs/WorkPackagePatch.md)
 - [WorkPackageRelationForm](docs/WorkPackageRelationForm.md)
 - [WorkPackageRelationFormModel](docs/WorkPackageRelationFormModel.md)
 - [WorkPackageSchema](docs/WorkPackageSchema.md)
 - [WorkPackageSchemas](docs/WorkPackageSchemas.md)
 - [WorkPackageSchemasModel](docs/WorkPackageSchemasModel.md)
 - [WorkPackages](docs/WorkPackages.md)
 - [WorkPackagesByProject](docs/WorkPackagesByProject.md)
 - [WorkPackagesByProjectModel](docs/WorkPackagesByProjectModel.md)
 - [WorkPackagesEmbedded](docs/WorkPackagesEmbedded.md)
 - [WorkPackagesModel](docs/WorkPackagesModel.md)


## Documentation for Authorization

Authentication schemes defined for the API:
### basicAuth

- **Type**: HTTP basic authentication


## Recommendation

It's recommended to create an instance of `ApiClient` per thread in a multithreaded environment to avoid any potential issues.

## Author



